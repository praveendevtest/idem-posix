#!/bin/bash

set -e
# base64 decode secret using the newly imported gpg private
AWS_SECRET_ACCESS_KEY="ABCDEfgHIJ3ree"

# Output secret as JSON
jq -n --arg secret "$AWS_SECRET_ACCESS_KEY" '{"secret": $secret}'
